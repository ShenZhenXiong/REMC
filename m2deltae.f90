
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
function m2deltae(latt,lattm,unum,us) result(m2de)
use para
implicit none
integer,intent(in)::latt,lattm
integer,intent(out)::unum,us
double precision::m2de
double precision::prod
integer::i,m
i=latt
m=lattm
unum=latt

if(m==1.or.m==2)then
dlamda=j31*(prod(dspin(1,i,m),spin(1,i,8))-prod(dspin(1,i,m),spin(1,l(i),6)))
us=1

else if(m==3.or.m==4)then
dlamda=j31*(prod(dspin(1,i,m),spin(1,i,5))-prod(dspin(1,i,m),spin(1,d(i),7)))
us=2

else if(m==5)then
dlamda=j31*(prod(spin(1,i,3),dspin(1,i,5))+prod(spin(1,i,4),dspin(1,i,5)))
us=2

else if(m==6)then
dlamda=-1.*j31*(prod(spin(1,r(i),1),dspin(1,i,6))+prod(spin(1,r(i),2),dspin(1,i,6)))
us=1
unum=r(i)

else if(m==7)then
dlamda=-1.*j31*(prod(spin(1,u(i),3),dspin(1,i,7))+prod(spin(1,u(i),4),dspin(1,i,7)))
unum=u(i)
us=2

else
dlamda=j31*(prod(spin(1,i,1),dspin(1,i,8))+prod(spin(1,i,2),dspin(1,i,8)))
us=1

end if
m2de=dlamda*udis(unum,us)

end function


!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
