subroutine recursion 
use para
implicit none
!include 'mpif.h'
double precision::wk_temp0,sum_temp,lamdam
double precision,allocatable::beta_temp(:),exacpt_temp(:)
integer::i,j
double precision::weigh_scale=0.5

allocate(beta_temp(0:nproc-1))
allocate(exacpt_temp(0:nproc-2))

do i=0,nproc-2
exacpt_temp(i)=exacpt(i)
end do

wk_temp0=1.d0*minval(exacpt_temp)
!if(wk_temp0<1.0)wk_temp0=1.0e-5
wk=wk_temp+wk_temp0

do i=0,nproc-1
wb(i)=wb_temp(i)+wk_temp0*beta(i)
!beta(i)=wb(i)/wk
end do
wk_temp=wk
wb_temp(:)=wb(:)

sum_temp=0.d0
do i=1,nproc-1
sum_temp=sum_temp+(beta(i)-beta(i-1))*exacpt(i-1)
end do
lamdam=(beta(nproc-1)-beta(0))/sum_temp
beta_temp(0)=beta(0)

do i=1,nproc-1
beta_temp(i)=beta_temp(i-1)+(1.0-weigh_scale+weigh_scale*exacpt(i-1)*lamdam)*(beta(i)-beta(i-1))
end do
beta(:)=beta_temp(:)

!end subroutine 

if(my_rank==1)then
write(1000,*)'beta value of the',irec,'recursion' 
do i=0,nproc-1
write(1000,'(i5,f15.5)')i,beta(i)
end do
end if

if(irec==nrec)then
    beta(:)=wb(:)/wk
    if(my_rank==1)then
       write(1000,*)'beta value of the last ',irec,'recursion' 
       do i=0,nproc-1
       write(1000,'(i5,f15.5)')i,beta(i)
       end do
    end if
end if
end subroutine


