!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!more complicated form of umod is added ,but only j3' is considered
function udeltael(unum,us) result(ude)
use para
implicit none
integer,intent(in)::unum,us
double precision::ude
double precision,external::cauchyul

deltau=cauchyul()
unew(unum,us)=udis(unum,us)+deltau
ude=lamda(unum,us)*deltau+(unew(unum,us)+udis(unum,us))*deltau*(omig-2.d0*jxy-jz)

if(us==1)then
ude=ude+deltau*(jxy*(udis(unum,2)+udis(u(unum),2)+udis(l(unum),2)+udis(lu(unum),2))+jz*(udis(uz(unum),1)+udis(dz(unum),1)))
else if(us==2)then
ude=ude+deltau*(jxy*(udis(unum,1)+udis(d(unum),1)+udis(r(unum),1)+udis(rd(unum),1))+jz*(udis(uz(unum),2)+udis(dz(unum),2)))
end if

end function
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&


!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!more complicated form of umod is added ,but only j3' is considered
function udeltaeb(unum,us) result(ude)
use para
implicit none
integer,intent(in)::unum,us
double precision::ude
double precision,external::cauchyub

deltau=cauchyub()
unew(unum,us)=udis(unum,us)+deltau
ude=lamda(unum,us)*deltau+(unew(unum,us)+udis(unum,us))*deltau*(omig-2.d0*jxy-jz)

if(us==1)then
ude=ude+deltau*(jxy*(udis(unum,2)+udis(u(unum),2)+udis(l(unum),2)+udis(lu(unum),2))+jz*(udis(uz(unum),1)+udis(dz(unum),1)))
else if(us==2)then
ude=ude+deltau*(jxy*(udis(unum,1)+udis(d(unum),1)+udis(r(unum),1)+udis(rd(unum),1))+jz*(udis(uz(unum),2)+udis(dz(unum),2)))
end if


end function
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!relax the local phonon related to the changed spin
subroutine ulocal(unum,us)
use para
implicit none
integer,intent(in)::unum,us
double precision,external::udeltael,ran1
integer::ennumu1,ennumu2
double precision::linkude,tote2u,rateu
integer::ucount
  
  do ucount=1,ultime
      
        linkude=udeltael(unum,us)
        tote2u=tote+linkude

        ucountl=ucountl+1
        if(linkude<0.0.or.exp(-linkude*beta(mybeta_num))>ran1(seed1))then
!        rateu=exp(bmu(ennumu1)*tote-amu(ennumu1)-bmu(ennumu2)*tote2u+amu(ennumu2))
!        if(rateu>=1.0.or.rateu>ran1(seed1))then
                   
                 uacceptl=uacceptl+1
                 !if(sweepcount>npre(tflag))then
                 totu=totu+deltau
                 tote=tote+linkude
                 !end if

                 udis(unum,us)=unew(unum,us)
           
        end if
                  if(uadap==1.and.ucountl==ulen)then
                  probl=1.0*uacceptl/ucountl 
                  if(probl>pu)deltaul=deltaul*(1.0+beta0*(probl-pu)/(1.0-pu))
                  if(probl<=pl)deltaul=deltaul/(1.0+beta1*(pl-probl)/pl)              
                  ucountl=0
                  uacceptl=0
                  end if
  end do

end subroutine

!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
subroutine uback
use para
implicit none
double precision,external::udeltaeb,ran1
double precision::linkude,tote2u,rateu
integer::unumb,usb
integer::ennumu1,ennumu2
                 
		 unumb=int(ran1(seed1)*nlatt)+1
                 usb=int(ran1(seed1)*nmod)+1
                 ucountb=ucountb+1
                 linkude=udeltaeb(unumb,usb)
                 tote2u=tote+linkude

                 if(linkude<0.0.or.exp(-linkude*beta(mybeta_num))>ran1(seed1))then

                     uacceptb=uacceptb+1

                     !if(sweepcount>npre(tflag))then
                     totu=totu+deltau
                     tote=tote+linkude
                     !end if

                     udis(unumb,usb)=unew(unumb,usb)
                 end if

                    if(uadap==1.and.ucountb==ulen)then
                    probb=1.0*uacceptb/ucountb 
                    if(probb>pu)deltaub=deltaub*(1.0+beta0*(probb-pu)/(1.0-pu))
                    if(probb<=pl)deltaub=deltaub/(1.0+beta1*(pl-probb)/pl)              
                    ucountb=0
                    uacceptb=0
                    end if

end subroutine

!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
