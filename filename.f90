module mod_filename

contains
!This program is used to generate a series of files with sequencial filenames
!i.e. file001, file002, ..., file***
 function filename(filenum)
 implicit none
 character(len=3)::filename
 integer,intent(in)::filenum
 integer::filenum1,filenum2,filenum3
 !character(*),intent(in)::string
 if(filenum>999)then
 write(*,*)'the filenum is too large'
 stop
 end if

 filenum1=filenum/100
 filenum2=(filenum-filenum1*100)/10
 filenum3=(filenum-filenum1*100-filenum2*10)
 filename=achar(filenum1+48)//achar(filenum2+48)//achar(filenum3+48)

 end function

end module
