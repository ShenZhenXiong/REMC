
subroutine initialization
use para 
implicit none
double precision,external::ran1
integer::i,j,a,b
double precision::norm
character(len=1)::initials,initialu
integer,save::seed_is
integer,save::seed_iu

seed_is=seedx
seed_iu=seedx

initials='r'
initialu='r'
!initialize the spin configration
if(initials=='r')then
do i=1,nlatt
  do j=1,natom
  spin(1,i,j)=2.0*ran1(seed_is)-1.0
  spin(2,i,j)=2.0*ran1(seed_is)-1.0
  spin(3,i,j)=2.0*ran1(seed_is)-1.0
  norm=sqrt(spin(1,i,j)**2.0+spin(2,i,j)**2.0+spin(3,i,j)**2.0)
  spin(1,i,j)=spin(1,i,j)/norm
  spin(2,i,j)=spin(2,i,j)/norm
  spin(3,i,j)=spin(3,i,j)/norm
  end do
end do


end if

!initialize the umode displacement
!randomly initialized
if(initialu=='r')then
do a=1,nlatt
  do b=1,nmod

  udis(a,b)=umax*(2.0*ran1(seed_iu)-1.0)

  end do
end do
!0 state 
else if(initialu=='g')then
do a=1,nlatt
   do b=1,nmod
   !if(mod(b,2)==1)then
   udis(a,b)=0.0
   !else
   !udis(a,b)=-5.0
   !end if
   end do
end do
else if(initialu=='c')then

do a=1,nlatt
   do b=1,nmod
   if(b==1)then
   udis(a,b)=1.d0
   else
   udis(a,b)=1.d0
   end if
   end do
end do

end if
end subroutine


!transform the initial spherical coordinates to the cartesian coordinates
subroutine config_init
use para
implicit none

integer::i,j,k
 character(len=3)::filename
 integer::filenum1,filenum2,filenum3
 filenum1=my_rank/100
 filenum2=(my_rank-filenum1*100)/10
 filenum3=(my_rank-filenum1*100-filenum2*10)
 filename=achar(filenum1+48)//achar(filenum2+48)//achar(filenum3+48)


if(initialsflag==0.and.initialuflag==0)then

 call initialization

else 

if(initialsflag==1)then

 open(2000,file='./configin/spin'//filename//'.dat')
do i=1,nlatt
  do k=1,natom
  read(2000,*)spin(1,i,k),spin(2,i,k),spin(3,i,k)
  end do
end do
write(*,*)'reading the initial spin configration is ok'
 close(2000)
end if

if(initialuflag==1)then

 open(3000,file='./configin/udisp'//filename//'.dat')
do i=1,nlatt
 read(3000,*)udis(i,1),udis(i,2)
end do
write(*,*)'reading the initial phonon configration is ok'
end if

 close(3000)

end if

end subroutine
