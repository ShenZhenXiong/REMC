#!/bin/bash
#
#PBS -l nodes=8:ppn=15
#PBS -l walltime=480:00:00
#PBS -j oe
#PBS -V
#
ulimit -s unlimited
export OMP_NUM_THREADS=1
cd $PBS_O_WORKDIR
NP=`cat $PBS_NODEFILE | wc -l`
EXEC=./mc.exe
mpirun -machinefile $PBS_NODEFILE -np $NP $EXEC > Log.txt 
