
!this subroutine is used to locate the spin in the lattice
subroutine latticedefine
use para
implicit none
integer::i,j,k,n
!mark the lattice by label lattice(i,j,k)
n=0
do k=1,dimz
  do j=1,dimy
    do i=1,dimx
        n=n+1
        lattice(i,j,k)=n
    end do
  end do
end do
end subroutine


subroutine neighbor
use para
implicit none
integer::i,j,k,l1,m,n

do i=1, dimx, 1
    do l1=-mmldx,mmldx,1
       di(l1,i)=i+l1
       if((i+l1).le.0) di(l1,i)=l1+i+dimx
       if((i+l1).gt.dimx) di(l1,i)=l1+i-dimx
    end do
end do

do j=1,dimy, 1
    do m=-mmldy, mmldy, 1
        dj(m,j) = j+m 
        if((j+m).le.0) dj(m,j)=m+j+dimy
        if((j+m).gt.dimy) dj(m,j)=m+j-dimy
    end do
end do

do k=1,dimz,1
    do n=-mmldz,mmldz,1
        dk(n,k)=k+n
        if((k+n).le.0) dk(n,k)=n+k+dimz
        if((k+n).gt.dimz) dk(n,k)=n+k-dimz
    end do
end do

do i=1,dimx,1
    do j=1,dimy,1
        do k=1,dimz,1
            do l1=-mmldx,mmldx, 1
                do m=-mmldy,mmldy,1
                    do n=-mmldz,mmldz,1
                        mld(n,m,l1,lattice(i,j,k))=&
                        &lattice(di(l1,i),dj(m,j),dk(n,k))
                    end do !n
                end do !m
            end do !l
        end do !z
    end do !j
end do !i

end subroutine neighbor


!this subroutine is used to define the neighbor lattice 
!subroutine neighbor
!use para
!implicit none
!integer::i,j,k
!
!do i=1,dimx
!di(i)=i-1
!ui(i)=i+1
!if(i==1)di(i)=dimx
!if(i==dimx)ui(i)=1
!end do
!
!do j=1,dimy
!dj(j)=j-1
!uj(j)=j+1
!if(j==1)dj(j)=dimy
!if(j==dimy)uj(j)=1
!end do
!
!do k=1,dimz
!dk(k)=k-1
!uk(k)=k+1
!if(k==1)dk(k)=dimz
!if(k==dimz)uk(k)=1
!end do
!
!do i=1,dimx
!  do j=1,dimy
!    do k=1,dimz
!        u(lattice(i,j,k))=lattice(i,uj(j),k)
!	d(lattice(i,j,k))=lattice(i,dj(j),k)
!	l(lattice(i,j,k))=lattice(di(i),j,k)
!        r(lattice(i,j,k))=lattice(ui(i),j,k)
!        uz(lattice(i,j,k))=lattice(i,j,uk(K))
!	dz(lattice(i,j,k))=lattice(i,j,dk(k))
!	lu(lattice(i,j,k))=lattice(di(i),uj(j),k)
!	ru(lattice(i,j,k))=lattice(ui(i),uj(j),k)
!	ld(lattice(i,j,k))=lattice(di(i),dj(j),k)
!	rd(lattice(i,j,k))=lattice(ui(i),dj(j),k)
!
!        udz(lattice(i,j,k))=lattice(i,uj(j),dk(k))
!        uuz(lattice(i,j,k))=lattice(i,uj(j),uk(k))
!
!        ddz(lattice(i,j,k))=lattice(i,dj(j),dk(k))
!        duz(lattice(i,j,k))=lattice(i,dj(j),uk(k))
!
!        ldz(lattice(i,j,k))=lattice(di(i),j,dk(k))
!        luz(lattice(i,j,k))=lattice(di(i),j,uk(k))
!
!        ruz(lattice(i,j,k))=lattice(ui(i),j,uk(k))
!        rdz(lattice(i,j,k))=lattice(ui(i),j,dk(k))
!
!        ludz(lattice(i,j,k))=lattice(di(i),uj(j),dk(k))
!        luuz(lattice(i,j,k))=lattice(di(i),uj(j),uk(k))
!
!        rudz(lattice(i,j,k))=lattice(ui(i),uj(j),dk(k))
!        ruuz(lattice(i,j,k))=lattice(ui(i),uj(j),uk(k))
!
!        lddz(lattice(i,j,k))=lattice(di(i),dj(j),dk(k))
!        lduz(lattice(i,j,k))=lattice(di(i),dj(j),uk(k))
!
!        rddz(lattice(i,j,k))=lattice(ui(i),dj(j),dk(k))
!        rduz(lattice(i,j,k))=lattice(ui(i),dj(j),uk(k))
!
!	end do
!  end do
!end do
!
!
!end subroutine
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
