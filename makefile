#FC= /export/data1/mpich-intel/bin/mpif90
#FC=/opt/mvapich-1.2/bin/mpif90
#FC=/opt/intel/impi/openmpi/bin/mpif90
#FC=/usr/mpi/intel/openmpi-1.4.3/bin/mpif90
#FC=/public/apps/openmpi-165-intel/bin/mpif90
FC=mpiifort


OBJ := .parameter.o .filename.o .distribution.o .seed_gen.o .replica_init.o .initialization.o .lattice.o  .initial.o \
      \
	.m1deltae.o .delta.o .renewm.o .metropolis.o .recursion.o .spinout.o .exchange.o .replica_ex.o 

mc.exe:	$(OBJ)
	$(FC)  -o$@ $^
.%.o :	%.f90
	$(FC) -o$@ -c $(<)
clean:
	rm .*o mc.exe
