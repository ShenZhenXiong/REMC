subroutine replica_init
use para
implicit none
integer::i,j,k
integer::inttemp

 allocate(beta(0:nproc-1))
 allocate(exacpt(0:nproc-1))
 allocate(excount(0:nproc-1))
 allocate(process(0:nproc-1))
 allocate(betanum(0:nproc-1))
 allocate(wb(0:nproc-1))
 allocate(wb_temp(0:nproc-1))
!the initial beta value in the recursion

if(initial_beta==0)then
  do i=0,nproc-1
  beta(i)=betamin+i*(betamax-betamin)/(nproc-1)
  if (my_rank ==0) write(*, *) i, beta(i) ! 20190319
  end do
else
  open(600,file='beta_init.dat')
  do i=0,nproc-1
  read(600,*)k,beta(i)
  end do
  close(600)
end if

!the initial beta distribution
if(initial_bedis==0)then
   do j=0,nproc-1
   process(j)=j
   betanum(j)=j
   end do
else
   open(5000,file='beta_dis.dat')
   do i=0,nproc-1
     read(5000,*)inttemp,betanum(i),process(i)

     if(inttemp/=i)then
     write(*,*)'the initial beta and process distribution is error'
     call abort
     end if

   end do

!check whether the distribution is right
   do i=0,nproc-1
   if(betanum(process(i))/=i)then
   write(*,*)'the initial beta and process distribution dismatch'
   call abort
   end if
   end do

   close(5000)
   write(*,*)'reading the initial beta and process distribution is ok'
end if

!the initialization of the recursion 
wk_temp=0.0
wb_temp(:)=0.0

 call ex_scheme

end subroutine


!determine the scheme of exchange beta
subroutine ex_scheme
use para
implicit none
integer::i,ibeta0,j

allocate(exbeta(0:nscheme-1,0:nproc-1))
exbeta(:,:)=-1
do i=0,nscheme-1
   ibeta0=i
   j=0
   do while(ibeta0<(nproc-1))
   exbeta(i,j)=ibeta0
   ibeta0=ibeta0+nscheme
   j=j+1
   end do
end do

if(my_rank==0)then
do i=0,nscheme-1
  write(*,*)i,'scheme'
  do j=0,nproc-1
  write(*,*)exbeta(i,j)
  end do
end do
end if

end subroutine









