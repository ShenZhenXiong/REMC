
!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!this subroutine is used to renew the spin configration and related quantity
subroutine renewm(latt,lattm)
use para
implicit none
integer,intent(in)::latt,lattm

!theta(latt,lattm)=newtheta(latt,lattm)
!phi(latt,lattm)=newphi(latt,lattm)

spin(:,latt,lattm)=newspin(:,latt,lattm)

end subroutine


!&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&