 program replica_ex
 use para
 use mpi
 implicit none
! include 'mpif.h'

 integer::i
 integer status(MPI_STATUS_SIZE)
 integer::one=1


 open(10000, file="./running_log.dat")
 call MPI_INIT(ierr)
 call MPI_Comm_rank(MPI_COMM_WORLD,my_rank,ierr)
 call MPI_Comm_size(MPI_COMM_WORLD, nproc, ierr)
      
if(my_rank == 0) then
    write(10000, "(A50)") "*************************************************"
    write(10000, "(A50)") "*                                               *"
    write(10000, "(A50)") "*   WELCOME TO REPLICA MONTE CARLO SIMULATION   *"
    write(10000, "(A50)") "*                                               *"
    write(10000, "(A50)") "*     EDIT BY CAO KUN AND SHEN ZHEN XIONG       *"
    write(10000, "(A50)") "*                                               *"
    write(10000, "(A50)") "*************************************************"
    write(10000, *) "Process number:  ", nproc
    
    call system("test -d output || mkdir output") 
    call system("test -d outdata || mkdir outdata")
    call system("test -d configout || mkdir configout")
    call system("test -d configin || mkdir configin")

  end if

  call MPI_Barrier(MPI_COMM_WORLD, ierr)

 call seed_gen
 
 seed1 = seed(my_rank)
 seedx = seed1-1234
 call replica_init

 call exchange
 
 call MPI_Barrier(MPI_COMM_WORLD, ierr)
 if(my_rank==0) write(10000,"(A21)") "*****RUNNING END*****"
 call MPI_Finalize(ierr)
 
 close(10000) 

end program
