subroutine seed_gen
use para
implicit none
!include 'mpif.h'

double precision,external::ran1
integer::i,j
integer::seed0=-2340
integer::testflag=0
!integer istatus(MPI_STATUS_SIZE)

!open(300,file='test.dat')

 allocate(seed(0:nproc-1))
do i=0,nproc-1
seed(i)=int(-59370*ran1(seed0))
!if (my_rank == 0) write(*,"(A4, 3X, I3, 3X,  A3, 3X, I7)") "seed ", i, "is: ", seed(i) ! 20190319 test ok
end do

do i=0,nproc-1
 do j=0,nproc-1
 if(i/=j.and.seed(i)==seed(j))then
 write(*,*)'initial seed error'
 end if
 end do
end do
 
end subroutine 
 
